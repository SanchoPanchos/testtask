package com.alex.fileexhibitsloader

import android.content.Context
import com.alex.model.Exhibit
import com.alex.model.ExhibitResponse
import com.alex.model.ExhibitsLoader
import com.google.gson.Gson
import java.io.IOException
import java.io.InputStream


class FileExhibitsLoader(private val context: Context) : ExhibitsLoader {

    override fun getExhibitList(): List<Exhibit> {
        try {
            val inputStream: InputStream = context.assets.open("exhibitsList.json")
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()

            val text = String(buffer)
            return Gson().fromJson(text, ExhibitResponse::class.java).exhibitsList
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return listOf()
    }

}