package com.alex.model

import com.google.gson.annotations.SerializedName

class Exhibit(
    @SerializedName("title") val title: String,
    @SerializedName("images") val images: List<String>
)
