package com.alex.model

import com.google.gson.annotations.SerializedName

class ExhibitResponse(@SerializedName("list") val exhibitsList : List<Exhibit>)