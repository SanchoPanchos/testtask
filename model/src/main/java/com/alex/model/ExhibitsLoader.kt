package com.alex.model

interface ExhibitsLoader {

    fun getExhibitList():List<Exhibit>

}