package com.alex.testapp

import android.app.Application
import com.alex.testapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@TestApplication)
            modules(appModule)
        }
    }


}