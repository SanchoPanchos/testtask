package com.alex.testapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alex.testapp.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_exhibit_image.view.*

class ExhibitsInnerAdapter : RecyclerView.Adapter<ExhibitsInnerAdapter.ViewHolder>() {

    private val imagesList: MutableList<String> = mutableListOf()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(imageUrl: String) {
            Glide.with(itemView.exhibitImage)
                .load(imageUrl).into(itemView.exhibitImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_exhibit_image, parent, false)
        return ViewHolder(v)
    }

    fun append(newImagesList : List<String>){
        val oldSize = imagesList.size
        imagesList.addAll(newImagesList)
        notifyItemRangeInserted(oldSize, newImagesList.size)
    }

    override fun getItemCount() = imagesList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(imagesList[position])
}