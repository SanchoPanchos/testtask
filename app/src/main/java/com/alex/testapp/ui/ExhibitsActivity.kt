package com.alex.testapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.alex.model.Exhibit
import com.alex.testapp.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class ExhibitsActivity : AppCompatActivity() {

    private val viewModel: ExhibitsViewModel by viewModel()
    private lateinit var adapter: ExhibitsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
        viewModel.exhibitsLiveData.observe(this, Observer { exhibitsList ->
            onExhibitsLoaded(exhibitsList)
        })
    }

    private fun initRecycler() {
        adapter = ExhibitsAdapter()
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
    }

    private fun onExhibitsLoaded(exhibitsList: List<Exhibit>) {
        adapter.append(exhibitsList)
    }

}
