package com.alex.testapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.alex.fileexhibitsloader.FileExhibitsLoader
import com.alex.model.Exhibit
import kotlinx.coroutines.Dispatchers

class ExhibitsViewModel(private val exhibitsLoader: FileExhibitsLoader) : ViewModel() {

    val exhibitsLiveData: LiveData<List<Exhibit>> = liveData(Dispatchers.IO) {
        emit(exhibitsLoader.getExhibitList())
    }

}