package com.alex.testapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alex.model.Exhibit
import com.alex.testapp.R
import kotlinx.android.synthetic.main.item_exhibit.view.*

class ExhibitsAdapter : RecyclerView.Adapter<ExhibitsAdapter.ExhibitsViewHolder>() {

    private val exhibitsList: MutableList<Exhibit> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExhibitsViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_exhibit, parent, false)
        return ExhibitsViewHolder(v)
    }

    override fun getItemCount() = exhibitsList.size

    override fun onBindViewHolder(holder: ExhibitsViewHolder, position: Int) =
        holder.bind(exhibitsList[position])


    fun append(newExhibits: List<Exhibit>) {
        val oldSize = exhibitsList.size
        exhibitsList.addAll(newExhibits)
        notifyItemRangeInserted(oldSize, newExhibits.size)
    }

    class ExhibitsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(exhibit: Exhibit) {
            itemView.exhibitTitle.text = exhibit.title
            itemView.exhibitRecycler.layoutManager =
                LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
            itemView.exhibitRecycler.adapter = ExhibitsInnerAdapter().apply {
                append(exhibit.images)
            }
        }
    }

}