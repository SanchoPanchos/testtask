package com.alex.testapp.di

import com.alex.fileexhibitsloader.FileExhibitsLoader
import com.alex.testapp.ui.ExhibitsViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel {
        ExhibitsViewModel(get())
    }

    single { FileExhibitsLoader(androidApplication()) }

}